(function(module){
    var Capitalize = function(){
        return function(input){

            if(input == undefined || input == null){
                return;
            }

            var names = input.split(' ');
            var nameCapitalize = [];
            angular.forEach(names, function(name) {
                nameCapitalize.push(name.charAt(0).toUpperCase() + name.slice(1).toLowerCase());
            });
            return nameCapitalize.join(' ');
        };
    };
    Capitalize.$inject = [];
    module.filter('capitalize', Capitalize);
})(angular.module('ngEhtCommons'));