(function(module){
	var AdvancedFilter = function($filter){
		return function(data, text){

			if(text == undefined || text == null){
                return data;
            }

	    	var textArr = text.split(' ');

	        angular.forEach(textArr, function(test){
	            if(test){
	            	data = $filter('filter')(data, test);
	            }
	        });

	        return data;
	    };
	};

	AdvancedFilter.$inject = ['$filter'];
	module.filter('advancedFilter', AdvancedFilter);
})(angular.module('ngEhtCommons'));