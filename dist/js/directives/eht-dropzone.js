(function(module){
	var EhtDropzone = function(){
		return {
			'restrict' : 'A',
			'replace' : true,
			'template' : '<div> ' +
				'	<div class="fallback">' +
				'		<div ng-show="!isLoading">' +
				'			<input type="file" class="filestyle" name="file"/>' +
				'		</div>' +
				'		<div ng-show="isLoading">' +
				'			<span ng-show="dictFallbackLoadingText">' +
				'				{{dictFallbackLoadingText}}' +
				'			</span>' +
				'			<span ng-show="!dictFallbackLoadingText">Loading ...</span>' +
				'		</div>' +
				'	</div>' +
				'</div>',
			'scope' : {
				"removeOnComplete" : '=',
				'success' : '=',
				'url' : '=', //Has to be specified on elements other than form (or when the form doesn't have an action attribute). You can also provide a function that will be called with files and must return the url (since v3.12.0)
				'method' : '=', //Defaults to "post" and can be changed to "put" if necessary. You can also provide a function that will be called with files and must return the method (since v3.12.0)
				'parallelUploads' : '=', //How many file uploads to process in parallel (See the Enqueuing file uploads section for more info)
				'maxFilesize' : '=', //in MB
				'filesizeBase' : '=', //Defaults to 1000. This defines whether a base of 1000 or 1024 should be used to calculate file sizes. 1000 is correct, because 1000 Bytes equal 1 Kilobyte, and 1024 Bytes equal 1 Kibibyte. You can change this to 1024 if you don't care about validity.
				'paramName' : '=', //The name of the file param that gets transferred. Defaults to file. NOTE: If you have the option uploadMultiple set to true, then Dropzone will append [] to the name.
				'uploadMultiple' : '=', //Whether Dropzone should send multiple files in one request. If this it set to true, then the fallback file input element will have the multiple attribute as well. This option will also trigger additional events (like processingmultiple). See the events section for more information.
				'headers' : '=', //An object to send additional headers to the server. Eg: headers: { "My-Awesome-Header": "header value" }
				'addRemoveLinks' : '=', //This will add a link to every file preview to remove or cancel (if already uploading) the file. The dictCancelUpload, dictCancelUploadConfirmation and dictRemoveFile options are used for the wording.
				'previewsContainer' : '=', //defines where to display the file previews – if null the Dropzone element is used. Can be a plain HTMLElement or a CSS selector. The element should have the dropzone-previews class so the previews are displayed properly.
				'clickable' : '=', //If true, the dropzone element itself will be clickable, if false nothing will be clickable. Otherwise you can pass an HTML element, a CSS selector (for multiple elements) or an array of those.
				'createImageThumbnails' : '=',	
				'maxThumbnailFilesize' : '=', //in MB. When the filename exceeds this limit, the thumbnail will not be generated.
				'thumbnailWidth' : '=', //if null, the ratio of the image will be used to calculate it.
				'thumbnailHeight' : '=', //the same as thumbnailWidth. If both are null, images will not be resized.
				'maxFiles' : '=', //if not null defines how many files this Dropzone handles. If it exceeds, the event maxfilesexceeded will be called. The dropzone element gets the class dz-max- files-reached accordingly so you can provide visual feedback.
				'resize' : '=', //is the function that gets called to create the resize information. It gets the file as first parameter and must return an object with srcX, srcY, srcWidth and srcHeight and the same for trg*. Those values are going to be used by ctx.drawImage().
				'init' : '=', //is a function that gets called when Dropzone is initialized. You can setup event listeners inside this function.
				'acceptedMimeTypes' : '=', //Deprecated in favor of acceptedFiles
				'acceptedFiles' : '=', //The default implementation of accept checks the file's mime type or extension against this list. This is a comma separated list of mime types or file extensions. Eg.: image/*,application/pdf,.psd. If the Dropzone is clickable this option will be used as accept parameter on the hidden file input as well.
				'accept' : '=', //is a function that gets a file and a done function as parameter. If the done function is invoked without a parameter, the file will be processed. If you pass an error message it will be displayed and the file will not be uploaded. This function will not be called if the file is too big or doesn't match the mime types.
				'enqueueForUpload' : '=', //Deprecated in favor of autoProcessQueue.
				'autoProcessQueue' : '=', //When set to false you have to call myDropzone.processQueue() yourself in order to upload the dropped files. See below for more information on handling queues.
				'previewTemplate' : '=', //is a string that contains the template used for each dropped image. Change it to fulfill your needs but make sure to properly provide all elements. You can include the HTML in your page in a <div id="preview-template" style="display: none;"></div> container, and set it like this: previewTemplate: document.querySelector('preview-template').innerHTML.
				'forceFallback' : '=', //Defaults to false. If true the fallback will be forced. This is very useful to test your server implementations first and make sure that everything works as expected without dropzone if you experience problems, and to test how your fallbacks will look.
				'fallback' : '=', //is a function that gets called when the browser is not supported. The default implementation shows the fallback input field and adds a text.

				'dictDefaultMessage' : '=', //The message that gets displayed before any files are dropped. This is normally replaced by an image but defaults to "Drop files here to upload"
				'dictFallbackMessage' : '=', //If the browser is not supported, the default message will be replaced with this text. Defaults to "Your browser does not support drag'n'drop file uploads."
				'dictFallbackText' : '=', //This will be added before the file input files. If you provide a fallback element yourself, or if this option is null this will be ignored. Defaults to "Please use the fallback form below to upload your files like in the olden days."
				'dictInvalidFileType' : '=', //Shown as error message if the file doesn't match the file type.
				'dictFileTooBig' : '=', //Shown when the file is too big. {{filesize}} and {{maxFilesize}} will be replaced.
				'dictResponseError' : '=', //Shown as error message if the server response was invalid. {{statusCode}} will be replaced with the servers status code.
				'dictCancelUpload' : '=', //If addRemoveLinks is true, the text to be used for the cancel upload link.
				'dictCancelUploadConfirmation' : '=', //If addRemoveLinks is true, the text to be used for confirmation when cancelling upload.
				'dictRemoveFile' : '=', //If addRemoveLinks is true, the text to be used to remove a file.
				'dictMaxFilesExceeded' : '=', //If maxFiles is set, this will be the error message when it's exceeded.
			
				'dictFallbackButtonText' : '=',
				'fallbackButtonName' : '@',
				'dictFallbackLoadingText' : '='
			},
			'link' : function(scope, element, attrs){
				console.log('Loading eht dropzone directive');
				var settings = {};
				settings.url = '/ehealthtracker-rest/rest/file';

				if(scope.url) settings.url = scope.url;
				if(scope.method) settings.method = scope.method;
				if(scope.parallelUploads) settings.parallelUploads = scope.parallelUploads;
				if(scope.maxFilesize) settings.maxFilesize = scope.maxFilesize;
				if(scope.filesizeBase) settings.filesizeBase = scope.filesizeBase;
				if(scope.paramName) settings.paramName = scope.paramName;
				if(scope.uploadMultiple) settings.uploadMultiple = scope.uploadMultiple;
				if(scope.headers) settings.headers = scope.headers;
				if(scope.addRemoveLinks) settings.addRemoveLinks = scope.addRemoveLinks;
				if(scope.previewsContainer) settings.previewsContainer = scope.previewsContainer;
				if(scope.clickable) settings.clickable = scope.clickable;
				if(scope.createImageThumbnails) settings.createImageThumbnails = scope.createImageThumbnails;
				if(scope.maxThumbnailFilesize) settings.maxThumbnailFilesize = scope.maxThumbnailFilesize;
				if(scope.thumbnailWidth) settings.thumbnailWidth = scope.thumbnailWidth;
				if(scope.thumbnailHeight) settings.thumbnailHeight = scope.thumbnailHeight;
				if(scope.maxFiles) settings.maxFiles = scope.maxFiles;
				if(scope.resize) settings.resize = scope.resize;
				if(scope.acceptedMimeTypes) settings.acceptedMimeTypes = scope.acceptedMimeTypes;
				if(scope.acceptedFiles) settings.acceptedFiles = scope.acceptedFiles;
				if(scope.accept) settings.accept = scope.accept;
				if(scope.enqueueForUpload) settings.enqueueForUpload = scope.enqueueForUpload;
				if(scope.autoProcessQueue) settings.autoProcessQueue = scope.autoProcessQueue;
				if(scope.previewTemplate) settings.previewTemplate = scope.previewTemplate;
				if(scope.forceFallback) settings.forceFallback = scope.forceFallback;
				//'fallback' : '=', //is a function that gets called when the browser is not supported. The default implementation shows the fallback input field and adds a text.






				if(scope.dictDefaultMessage) settings.dictDefaultMessage = scope.dictDefaultMessage;
				if(scope.dictFallbackMessage) settings.dictFallbackMessage = scope.dictFallbackMessage;
				if(scope.dictFallbackText) settings.dictFallbackText = scope.dictFallbackText;
				if(scope.dictInvalidFileType) settings.dictInvalidFileType = scope.dictInvalidFileType;
				if(scope.dictFileTooBig) settings.dictFileTooBig = scope.dictFileTooBig;
				if(scope.dictResponseError) settings.dictResponseError = scope.dictResponseError;
				if(scope.dictCancelUpload) settings.dictCancelUpload = scope.dictCancelUpload;
				if(scope.dictCancelUploadConfirmation) settings.dictCancelUploadConfirmation = scope.dictCancelUploadConfirmation;
				if(scope.dictRemoveFile) settings.dictRemoveFile = scope.dictRemoveFile;
				if(scope.dictMaxFilesExceeded) settings.dictMaxFilesExceeded = scope.dictMaxFilesExceeded;



				var $file = element.find('input');
				var buttonSettings = {};
				if(scope.dictFallbackButtonText) buttonSettings.buttonText = scope.dictFallbackButtonText;
				if(scope.fallbackButtonName) buttonSettings.buttonName = scope.fallbackButtonName;
				$file.filestyle(buttonSettings);

				$file.change(function(){
					scope.$apply(function(){
						scope.isLoading = true;	
					});					
					$.ajax(settings.url, {
						'files' : $file,
						'iframe' : true
					}).complete(function(data){						
						console.log('Data: ' + angular.toJson(data));
						scope.$apply(function(){
							scope.isLoading = false;
							scope.success({}, data.responseJSON.value);
						});
					});
				});

				scope.isLoading = false;

				console.log('Settings: ' + angular.toJson(settings));
				console.log('Button settings:' + angular.toJson(buttonSettings));

				settings.init = function(){
					if(scope.success){
						var dz = this;
    					this.on("success", function(file, response) { 
    						scope.$apply(function(){
    							scope.success(file, response);	
								if(scope.removeOnComplete)
									dz.removeFile(file);
    						});    						
    					});
    				}
				};


				element.dropzone(settings);
			}
		};
	};
	module.directive('ehtDropzone', EhtDropzone);
})(angular.module('ngEhtCommons'));