(function(module){
	var EhtDropdownDatepicker = function(){
		return {
			'restrict' : 'A',
			'replace' : 'true',
			'template' : '<div class="row">'+
			'	<div class="col-xs-4 form-group form-group-sm">	'+
			'		<label class="col-xs-12 text-center">{{dateLabel}}</label>		'+
			'		<select ng-model="selectedDay" '+
			'			class="form-control input-sm"'+ 
			'			ng-change="updateValue()"'+
			'			ng-options="day for day in range(maxDays)">'+
			'			<option value="">{{dateLabel}}</option>'+
			'		</select>'+
			'	</div>'+
			'	<div class="col-xs-4 form-group form-group-sm">'+
			'		<label class="col-xs-12 text-center">{{monthLabel}}</label>		'+
			'		<select ng-model="selectedMonth" '+
			'			class="form-control input-sm"'+
			'			ng-change="monthOrYearChanged()" '+
			'			ng-options="month.id as month.name for month in months">			'+
			'			<option value="">{{monthLabel}}</option>'+
			'		</select>'+
			'	</div>'+
			'	<div class="col-xs-4 form-group form-group-sm">'+
			'		<label class="col-xs-12 text-center">{{yearLabel}}</label>			'+
			'		<select ng-model="selectedYear" '+
			'			class="form-control input-sm" '+
			'			ng-change="monthOrYearChanged()"'+
			'			ng-options="year for year in years">'+
			'			<option value="">{{yearLabel}}</option>'+
			'		</select>'+
			'	</div>'+
			'</div>',
			'require' : 'ngModel',
			'scope' : {
				'months' : '=',
				'format' : '@'
			},
			'link' : function(scope, element, attrs, NgModelController){
				var maxDays = 31;
				var maxMonths = 12;
				var today = moment();
				var maxYear = today.year();
				var minYear = maxYear - 130;

				scope.maxDays = maxDays;
				scope.maxMonths = maxMonths;
				scope.maxYear = maxYear;
				scope.minYear = minYear;


				scope.dateLabel = attrs.dateLabel ? attrs.dateLabel : 'Date';
				scope.monthLabel = attrs.monthLabel ? attrs.monthLabel : 'Month';
				scope.yearLabel = attrs.yearLabel ? attrs.yearLabel : 'Year';

				scope.range = function(size){
					var days = [];
					var counter = 1;
					do{
						days.push(counter);
						counter++;
					}while(size >= counter);
					return days;
				};

				var months = [
					{ 'id' : 1, 'name' : 'January' }, 
					{ 'id' : 2, 'name' : 'February' }, 
					{ 'id' : 3, 'name' : 'March' }, 
					{ 'id' : 4, 'name' : 'April' }, 
					{ 'id' : 5, 'name' : 'May' }, 
					{ 'id' : 6, 'name' : 'June' }, 
					{ 'id' : 7, 'name' : 'July' }, 
					{ 'id' : 8, 'name' : 'August' }, 
					{ 'id' : 9, 'name' : 'September' }, 
					{ 'id' : 10, 'name' : 'October' }, 
					{ 'id' : 11, 'name' : 'November' }, 
					{ 'id' : 12, 'name' : 'December' }
				];

				var years = [];
				do{
					years.push(maxYear);
					maxYear --;
				}while(minYear <= maxYear);
				scope.years = years;

				if(typeof scope.months == 'undefined')
					scope.months = months;

				NgModelController.$formatters.push(function(modelValue){
					var viewValue = {
						'year' : '',
						'month' : '',
						'day' : ''
					};

					if(!modelValue) return viewValue;

					var theDate = '';

					if(scope.format)
						theDate = moment(modelValue, scope.format);
					else
						theDate = moment(modelValue);
					viewValue.year = theDate.year();
					viewValue.month = theDate.month() + 1;
					viewValue.day = theDate.date();

					return viewValue;
				});

				NgModelController.$render = function(){
					scope.selectedYear = NgModelController.$viewValue.year;
					scope.selectedMonth = NgModelController.$viewValue.month;
					scope.selectedDay = NgModelController.$viewValue.day;
				}; 

				NgModelController.$parsers.push(function(viewValue){
					var myMoment = moment();
					myMoment.year(viewValue.year);
					myMoment.month(viewValue.month);
					myMoment.date(viewValue.day);

					var modelValue = '';
					if(scope.format)
						modelValue = myMoment.format(scope.format);
					else
						modelValue = myMoment.format();
					return modelValue;
				});

				scope.updateValue = function(){
					if(!scope.selectedDay)return;
					if(!scope.selectedMonth)return;
					if(!scope.selectedYear)return;

					var viewValue = {
						'year' : parseInt(scope.selectedYear),
						'month' : parseInt(scope.selectedMonth) - 1,
						'day' : parseInt(scope.selectedDay)
					};

					NgModelController.$setViewValue(viewValue);
				};

				scope.monthOrYearChanged = function(){
					if(!scope.selectedYear) return;
					if(!scope.selectedMonth) return;
					var myMoment = moment();
					myMoment.month(parseInt(scope.selectedMonth - 1));
					myMoment.year(parseInt(scope.selectedYear));

					var currentDaySelected = scope.selectedDay;						
					scope.maxDays = myMoment.daysInMonth();
					if(currentDaySelected > scope.maxDays) scope.selectedDay = scope.maxDays;

					scope.updateValue();
				};
			}
		};
	};
	EhtDropdownDatepicker.$inject = [];
	module.directive('ehtDropdownDatepicker', EhtDropdownDatepicker);
})(angular.module('ngEhtCommons'));