(function(module){
	var EhtTooltip = function(){
		return {
			'restrict' : 'A',
			'replace' : false,
			'scope' : {

			},
			'link' : function(scope, element, attrs){
				var options = {};
				for(var attrName in attrs){
					var prefix = 'tooltip';
					if(attrName.indexOf(prefix) < 0) continue;

					var optionName = attrName.substr(attrName.indexOf(prefix) + prefix.length).toLowerCase();

					var attrValue = attrs[attrName];
					options[optionName] = attrValue;
				};
				element.tooltip(options);
			}
		};
	};
	EhtTooltip.$inject = [];
	module.directive('ehtTooltip', EhtTooltip);
})(angular.module('ngEhtCommons'));