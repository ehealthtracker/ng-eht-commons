(function(module){
	var EhtEnterPress = function(){
	    return function (scope, element, attrs) {
	        element.bind("keypress", function (event) {
	            if(event.which === 13) {
	                scope.$apply(function (){
	                    scope.$eval(attrs.ehtEnterPress);
	                });

	                event.preventDefault();
	            }
	        });
	    };
	};
	module.directive('ehtEnterPress', EhtEnterPress);
})(angular.module('ngEhtCommons'));