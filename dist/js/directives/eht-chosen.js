(function(module){
	var EhtChosenDirective = function(){
		return {
			'restrict' : 'A',
			'scope' : {
				'ehtModel' : '=',
				'ehtOptions' : '=',
				'allowSingleDeselect' : '=',
					// - original name: allow_single_deselect
					// - default value:	false	
					// - description : When set to true on a single select, Chosen adds a UI element which selects the first element (if it is blank).
				'disableSearch' : '=',
					// - original name: disable_search
					// - default value: false
					// - description: When set to true, Chosen will not display the search field (single selects only).
				'disableSearchThreshold' : '=',
					// - original name: disable_search_threshold
					// - default value: 0
					// - description: Hide the search input on single selects if there are fewer than (n) options.
				'enableSplitWordSearch' : '=',
					// - original name: enable_split_word_search
					// - default value: true
					// - description: By default, searching will match on any word within an option tag. 
									// Set this option to false if you want to only match on the entire text of an option tag.
				'inheritSelectClasses' : '=', 
					// - original name: inherit_select_classes
					// - default value: false
					// - description: When set to true, Chosen will grab any classes on the original select field and add them to Chosen’s container div.
				'maxSelectedOptions' : '=',
					// - original name: max_selected_options
					// - default value: Infinity
					// - description: Limits how many options the user can select. When the limit is reached, the chosen:maxselected event is triggered.
				'noResultsText' : '=',
					// - original name:  no_results_text
					// - default value: "No results match"
					// - description: The text to be displayed when no matching results are found. 
									// The current search is shown at the end of the text (e.g., No results match "Bad Search").
				'placeholderTextMultiple' : '=',
					// - original name: placeholder_text_multiple
					// - default value: "Select Some Options"
					// - description: The text to be displayed as a placeholder when no options are selected for a multiple select.
				'placeholderTextSingle' : '=',
					// - original name: placeholder_text_single
					// - default value: "Select an Option"
					// - description: The text to be displayed as a placeholder when no options are selected for a single select.
				'searchContains' : '=',
					// - original name: search_contains
					// - default value: false
					// - description: By default, Chosen’s search matches starting at the beginning of a word. 
									// Setting this option to true allows matches starting from anywhere within a word. 
									// This is especially useful for options that include a lot of special characters or phrases in ()s and []s.
				'singleBackstrokeDelete' : '=',
					// - original name: single_backstroke_delete
					// - default value: true
					// - description: By default, pressing delete/backspace on multiple selects will remove a selected choice. 
									// When false, pressing delete/backspace will highlight the last choice, and a second press deselects it.
				'width' : '=',
					// - original name: width	
					// - default value: Original select width.	
					// - description: The width of the Chosen select box. By default, Chosen attempts to match the width of the select box you are replacing. 
									// If your select is hidden when Chosen is instantiated, you must specify a width or the select will show up with a width of 0.
				'displayDisabledOptions' : '=',
					// - original name: display_disabled_options,
					// - default value: true
					// - description: By default, Chosen includes disabled options in search results with a special styling. 
									// Setting this option to false will hide disabled results and exclude them from searches.
				'displaySelectedOptions' : '=',
					// - original name: display_selected_options
					// - default value: true
					// - description: By default, Chosen includes selected options in search results with a special styling. 
									//Setting this option to false will hide selected results and exclude them from searches.

									//Note: this is for multiple selects only. In single selects, the selected result will always be displayed.
				'includeGroupLabelInSelected' : '=',
					// - original name: include_group_label_in_selected
					// - default value: false
					// - description: By default, Chosen only shows the text of a selected option. 
									// Setting this option to true will show the text and group (if any) of the selected option.
			},
			'link' : function($scope, element, attrs){

				var settings = {};

				if($scope.allowSingleDeselect)
					settings.allow_single_deselect = $scope.allowSingleDeselect;
				if($scope.disableSearch)
					settings.disable_search = $scope.disableSearch;
				if($scope.disableSearchThreshold)
					settings.disable_search_threshold = $scope.disableSearchThreshold;
				if($scope.enableSplitWordSearch)
					settings.enable_split_word_search = $scope.enableSplitWordSearch;
				if($scope.inheritSelectClasses)
					settings.inherit_select_classes = $scope.inheritSelectClasses;
				if($scope.maxSelectedOptions)
					settings.max_selected_options = $scope.maxSelectedOptions;
				if($scope.noResultsText)
					settings.no_results_text = $scope.noResultsText;
				if($scope.placeholderTextMultiple)
					settings.placeholder_text_multiple = $scope.placeholderTextMultiple;
				if($scope.placeholderTextSingle)
					settings.placeholder_text_single = $scope.placeholderTextSingle;
				if($scope.searchContains)
					settings. search_contains = $scope.searchContains;
				if($scope.singleBackstrokeDelete)
					settings.single_backstroke_delete = $scope.singleBackstrokeDelete;
				if($scope.width)
					settings.width = $scope.width;
				if($scope.displayDisabledOptions)
					settings.display_disabled_options = $scope.displayDisabledOptions;
				if($scope.displaySelectedOptions)
					settings.display_selected_options = $scope.displaySelectedOptions;
				if($scope.includeGroupLabelInSelected)
					settings.include_group_label_in_selected = $scope.includeGroupLabelInSelected;

				element.chosen(settings);

				$scope.$watch('ehtOptions', function(){
					element.trigger("chosen:updated");
				});

				$scope.$watch('ehtModel', function(){
					element.trigger("chosen:updated");
				});
			}
		};
	};
	EhtChosenDirective.$inject = [];
	module.directive('ehtChosen', EhtChosenDirective);
})(angular.module('ngEhtCommons'));