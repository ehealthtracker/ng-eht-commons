(function(module){
	var EhtAutocomplete = function(){
		return {
			'scope' : {
				'source' : '=',
				'sourceLabel' : '@',
				'sourceValue' : '@',
				'uiMinLength' : '='
			},
			'require' : 'ngModel',
			'restrict' : 'A',
			'link' : function(scope, element, attrs, NgModelCtrl){

				scope.name = '';
				scope.$watch('name', function(newValue){
					element.val(scope.name);
				});


				NgModelCtrl.$formatters.push(function(modelValue){
					var viewValue = {
						'id' : '',
						'name' : ''
					};

					if(!modelValue) return viewValue;

					if(typeof modelValue == 'string'){
						viewValue.name = modelValue;
					} else { 
						viewValue.name = modelValue.name;
						viewValue.id = modelValue.id;
					}

					return viewValue;
				});

				NgModelCtrl.$render = function(){
					scope.id = NgModelCtrl.$viewValue.id;
					scope.name = NgModelCtrl.$viewValue.name;
				};

				NgModelCtrl.$parsers.push(function(viewValue){
					return viewValue;
				});

				console.log('Linking autocomplete');
				var options = {};
				options.select = function(event, ui){
					var viewValue = ui.item;
					if(scope.sourceLabel){
						viewValue[scope.sourceLabel] = ui.item.label;
						viewValue[scope.sourceValue] = ui.item.value;
					}
					NgModelCtrl.$setViewValue(viewValue);

					if(typeof viewValue == 'string')
						element.val(viewValue);
					else if(scope.sourceLabel)
						element.val(viewValue[scope.sourceLabel]);
					else element.val(viewValue.label);
					return false;
				};
				if(scope.uiMinLength)
					options.minLength = scope.uiMinLength;
				options.source = function(request, response){
					var regex = new RegExp(request.term, 'i');					
					var matches = [];
					
					if(!scope.source || !scope.source.length){
						response(matches);
						return;
					}

					for(var i = 0; i < scope.source.length; i++){
						var item = scope.source[i];

						var sourceLabel = scope.sourceLabel ?
							item[scope.sourceLabel] : 
							item;

						console.log('Comparing term: ' + angular.toJson(request.term) + ', with; ' + sourceLabel);
						if(!regex.test(sourceLabel)) continue;
						console.log('Matches');
						var match = scope.sourceLabel ? {
								'label' : sourceLabel,
								'value' : item[scope.sourceValue]
							} : sourceLabel;

						matches.push(match);
					}
					console.log(matches);
					response(matches);
				};
				element.autocomplete(options);
			}
		};
	};
	module.directive('ehtAutocomplete', EhtAutocomplete);
})(angular.module('ngEhtCommons'));