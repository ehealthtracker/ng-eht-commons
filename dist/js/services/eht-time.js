(function(module){
	var EhtTime = function(){
		var service = {};
		/**
		* Parses a time string in format HH:mm:ss.SSSSSSS
		* return - {hours,minutes,seconds}
		*/
		service.parseTimeString = function(timeString){
			var time = moment(timeString, 'HH:mm:ss.SSSSSSS');
			return {
				'hours' : time.hours(),
				'minutes' : time.minutes(),
				'seconds' : time.seconds()
			};
		};
		return service;
	};
	EhtTime.$inject = [];
	module.factory('EhtTime', EhtTime);
})(angular.module('ngEhtCommons'));