'use strict';

/**
 * @ngdoc function
 * @name ngEhtCommonsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ngEhtCommonsApp
 */
angular.module('ngEhtCommonsApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
