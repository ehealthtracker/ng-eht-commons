(function(module){
	var DropdownDatepickerCtrl = function($scope){
		$scope.date = '1987-01-08';
	};
	DropdownDatepickerCtrl.$inject = ['$scope'];
	module.controller('DropdownDatepickerCtrl', DropdownDatepickerCtrl);
})(angular.module('ngEhtCommonsApp'));