(function(module){
	var CapitalizeCtrl = function($scope){
		$scope.person = {
			'name' : null
		};
	};
	CapitalizeCtrl.$inject = ['$scope'];
	module.controller('CapitalizeCtrl', CapitalizeCtrl);
})(angular.module('ngEhtCommonsApp'));