(function(module){
	var DatepickerCtrl = function($scope,$rootScope){
		$scope.date = '1987-01-08';
		$scope.variable = 'si';
		$scope.isChangeForOther = false;
		$scope.d = {};

		$scope.updateDate = function(){
			console.log('updateDate function');
			$scope.d.date2 = angular.copy($scope.date);
			$scope.isChangeForOther = true;
		};

		$scope.updateDateDos = function(){
			if($scope.variable == 'si' && $scope.isChangeForOther){
				console.log('updateDateDos function: ' + $scope.isChangeForOther);
				$rootScope.$broadcast('updateDate',{});
			}
			$scope.isChangeForOther = false;
		};

		$scope.$watch('d.date2', function(newValue, oldValue) {
		  $scope.isChangeForOther = false;
		});
	};
	DatepickerCtrl.$inject = ['$scope','$rootScope'];
	module.controller('DatepickerCtrl', DatepickerCtrl);
})(angular.module('ngEhtCommonsApp'));