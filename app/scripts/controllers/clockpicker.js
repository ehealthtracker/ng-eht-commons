(function(module){
	var ClockpickerCtrl = function($scope){
		$scope.time = '13:59';
	};

	ClockpickerCtrl.$inject = ['$scope'];
	module.controller('ClockpickerCtrl', ClockpickerCtrl);
})(angular.module('ngEhtCommonsApp'));