(function(module){
	var EhtEnterPresCtrl = function($scope, EhtSweetAlert){
		$scope.name = '';
		$scope.hola= function (){
			EhtSweetAlert.swal("Saludo", "Hola " + $scope.name, "success");
		};
	};
	EhtEnterPresCtrl.$inject = ['$scope', 'EhtSweetAlert'];
	module.controller('EhtEnterPresCtrl', EhtEnterPresCtrl);
})(angular.module('ngEhtCommonsApp'));