(function(module){
	var CatalogsCtrl = function($scope, EhtCatalogs, EhtQuestionnairesCatalog){
		$scope.clinicalStudiesHint = null;
		$scope.clinicalServicesHint = null;
		$scope.results = {};


		$scope.getCountries = function(){
			EhtCatalogs.getCountries().success(function(countries){
				$scope.results.countries = countries;
			});	
		};

		$scope.getClinicalStudiesByHint = function(){
			EhtCatalogs.getClinicalStudiesByHint($scope.clinicalStudiesHint)
			.then(function(response){
				$scope.results.clinicalStudies = response.data;
			});
		};

		$scope.getClinicalServicesByHint = function(){
			EhtCatalogs.getClinicalServicesByHint($scope.clinicalServicesHint)
			.then(function(response){
				$scope.results.clinicalServices = response.data;
			});
		};

		$scope.getDiabetesQuestionnaire = function(){
			EhtQuestionnairesCatalog.getDiabetesQuestionnaire().success(function(diabetesQuestionnaire){
				$scope.results.diabetesQuestionnaire = diabetesQuestionnaire;
			});	
		};

		$scope.getHeartQuestionnaire = function(){
			EhtQuestionnairesCatalog.getHeartQuestionnaire().success(function(heartQuestionnaire){
				$scope.results.heartQuestionnaire = heartQuestionnaire;
			});	
		};

		$scope.getMedicinesNamesByHint = function(){
			EhtCatalogs.getMedicinesNamesByHint($scope.medicineName).success(function(medicinesNames){
				$scope.results.medicinesNames = medicinesNames;
			});	
		};

		$scope.getMedicinesByHint = function(){
			EhtCatalogs.getMedicinesByHint('BUTILHIOSCINA/PARACETAMOL').success(function(medicines){
				$scope.results.medicines = medicines;
			});	
		};
	};
	CatalogsCtrl.$inject = ['$scope', 'EhtCatalogs', 'EhtQuestionnairesCatalog'];
	module.controller('CatalogsCtrl', CatalogsCtrl);
})(angular.module('ngEhtCommonsApp'));