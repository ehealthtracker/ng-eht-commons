(function(module){
	var AdvancedFilterCtrl = function($scope){
		$scope.filter = {
			'name' : null
		};

		$scope.users = [
			{
				'id' : 'tVivanco',
				'nombre': 'Tania Abigail',
				'apellidoPaterno' : 'Vivanco',
				'apellidoMaterno' : 'Romero',
				'edad' : 28,
				'sexo' : 'Femenino'
			},
			{
				'id' : ' rPenaloza',
				'nombre': 'Rodrigo',
				'apellidoPaterno' : 'Peñaloza',
				'apellidoMaterno' : 'Ochoa',
				'edad' : 24,
				'sexo' : 'Masculino'
			},
			{
				'id' : 'eMartinez',
				'nombre': 'Emmanuel',
				'apellidoPaterno' : 'Martinez',
				'apellidoMaterno' : 'Fernández',
				'edad' : 28,
				'sexo' : 'Masculino'
			},
			{
				'id' : 'mDamian',
				'nombre': 'Mauricio',
				'apellidoPaterno' : 'Damian',
				'apellidoMaterno' : 'Araoz',
				'edad' : 28,
				'sexo' : 'Masculino'
			},
			{
				'id' : 'iPerez',
				'nombre': 'Israel',
				'apellidoPaterno' : 'Perez',
				'apellidoMaterno' : 'Olivares',
				'edad' : 25,
				'sexo' : 'Masculino'
			},
			{
				'id' : 'rSantos',
				'nombre': 'Rafael',
				'apellidoPaterno' : 'Rosas',
				'apellidoMaterno' : '',
				'edad' : 24,
				'sexo' : 'Masculino'
			}
		];
	};
	AdvancedFilterCtrl.$inject = ['$scope'];
	module.controller('AdvancedFilterCtrl', AdvancedFilterCtrl);
})(angular.module('ngEhtCommonsApp'));