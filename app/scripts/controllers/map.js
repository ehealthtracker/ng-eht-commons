'use strict';

/**
 * @ngdoc function
 * @name ngEhtCommonsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ngEhtCommonsApp
 */
angular.module('ngEhtCommonsApp')
  .controller('MapCtrl', function ($scope) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.address = 'Baja California 200, colonia Roma Sur, Delegación Cuahutemoc, México Distrito Federal';
  });
