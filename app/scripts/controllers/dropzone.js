(function(module){
	var DropzoneCtrl = function($scope){
		$scope.onSuccess = function(file, response){
			console.log('on success');
			console.log('on success, file: ' + angular.toJson(file));
			console.log('on success, response: ' + angular.toJson(response));
		};
	};
	DropzoneCtrl.$inject = ['$scope'];
	module.controller('DropzoneCtrl', DropzoneCtrl);
})(angular.module('ngEhtCommonsApp'));