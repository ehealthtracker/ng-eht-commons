(function(module){
	var AutocompleteCtrl = function($scope){
		$scope.source = ['Hola', 'Como', 'Estas', 'El', 'Día', 'De', 'Hoy', 'Amigo'];
	};
	AutocompleteCtrl.$inject = ['$scope'];
	module.controller('AutocompleteCtrl', AutocompleteCtrl);
})(angular.module('ngEhtCommonsApp'));