(function(module){
	var RunDisabledDropzoneAutodiscover = function(){
		Dropzone.autoDiscover = false;
	};
	module.run(RunDisabledDropzoneAutodiscover);
})(angular.module('ngEhtCommons'));