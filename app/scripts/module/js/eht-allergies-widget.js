(function(module){
	var EhtAllergiesWidget = function(){
		return {
			restrict: 'E',
			template: '<div class="widget">'+
			'	<div class="widget-extra themed-background-dark">'+
			'		<h3 class="widget-content-light">'+
			'            Mis <strong>Alergias</strong>'+
			'       </h3>'+
			'	</div>'+
			'	<div class="list-group">'+
			'		<div class="alert alert-success" ng-show="allergies.length == 0">'+
			'            <strong><i class="fa fa-thumbs-o-up fa-2x"></i></strong> &iexcl;Muy bien!, no tenemos registradas alergias.'+
			'       </div>'+
			'		<div ng-repeat="allergy in allergies" class="list-group-item">'+
			'			<span class="pull-right">{{allergy.detectionDate}}</span>'+
			'			<h4 class="list-group-item-heading"><strong class="text-primary">{{allergy.allergen}}</strong></h4>'+
			'			<p class="list-group-item-text">{{allergy.reaction}} - {{allergy.description}}</p>'+
			'		</div>'+
			'	</div>'+
			'</div>',
			replace: true,
			scope: {},
			controller: ['$scope', function(scope){
				
			}],
			link: function(scope, element, attr){
				scope.allergies=[
					{
						'detectionDate' : '2015-01-03',
						'allergen' : 'Cloro',
						'reaction' : 'Urticaria',
						'description' : 'Muere si toma'
					}
				];
				scope.allergy = {};
				scope.allergy.statusCode = 0;
			}
		};
	};
	EhtAllergiesWidget.$inject = [];
	module.directive('ehtAllergiesWidget', EhtAllergiesWidget);
})(angular.module('ngEhtCommons'));