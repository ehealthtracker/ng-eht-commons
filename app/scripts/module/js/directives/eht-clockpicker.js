(function(module){
	var EhtClockpicker = function(){
		return {
			'restrict' : 'A',
			'scope' : {
				'default' : '@',
					// - default value:	''	
					// - description : default time, 'now' or '13:14' e.g.
				'placement' : '@',
					// - default value:	'bottom'	
					// - description : popover placement
				'align' : '@',
					// - default value:	'left'	
					// - description : popover arrow align
				'autoclose' : '@',
					// - default value:	false
					// - description : auto close when minute is selected
				'vibrate' : '@'
					// - default value:	true	
					// - description : vibrate the device when dragging clock hand
			},
			'link' : function(scope, element, attrs){
				var clockpickerConfig = {};

				if(attrs.default) clockpickerConfig.default = attrs.default;
				if(attrs.placement) clockpickerConfig.placement = attrs.placement;
				if(attrs.align) clockpickerConfig.align = attrs.align;
				if(attrs.autoclose) clockpickerConfig.autoclose = attrs.autoclose;
				if(attrs.vibrate) clockpickerConfig.vibrate = attrs.vibrate;

				element.clockpicker(clockpickerConfig);
			}
		}
	};
	module.directive('ehtClockpicker', EhtClockpicker);
})(angular.module('ngEhtCommons'));