(function(module){
	var EhtDatepicker = function(){
		return {
			'scope' : {
				'format' : '@',
				'autoclose' : '=',
				'minViewMode' : '=',
				'endDate' : '@',
				'startDate' : '@',
				'todayHighlight' : '@',
				'orientation' : '@',
				'date' : '=',
				'zIndex': '@'
			},
			'restrict' : 'A',
			'link' : function(scope, element, attrs){
				var config = { };
				if(attrs.format) config.format = attrs.format;
				if(scope.autoclose) config.autoclose = scope.autoclose;
				if(scope.minViewMode) config.minViewMode = scope.minViewMode;
				if(scope.endDate) config.endDate = scope.endDate;
				if(scope.startDate) config.startDate = scope.startDate;
				if(attrs.todayHighlight) config.todayHighlight = attrs.todayHighlight;
				if(attrs.orientation) config.orientation = attrs.orientation;
				if(scope.zIndex)config.zIndex = parseInt(scope.zIndex);

				config.language = 'es';
				element.datepicker(config);

				if(scope.date){
					element.datepicker('update', scope.date);
				}

				
				scope.updateDate = function(){
			        element.datepicker('update', scope.date);
			    };
			    
			    scope.$on('updateDate',function(event, data){
			        scope.updateDate();
			    });
			    
/*
			    scope.$watch('startDate', function(){
			    	// console.log('Start Date changed for date picker - '+scope.startDate);
			    	// console.log('Updating datepicker config');
			    	var minDate = new Date(scope.startDate);
			    	console.log('minDate: '+minDate);
			    	element.datepicker("destroy");
			    	config.startDate = scope.startDate;
			    	element.datepicker('update');
			    	//element.datepicker('setStartDate', minDate);
			    	//element.datepicker(config);
			    	//console.log('Datepicker updated');
			    });
					*/
			}
		};
	};
	module.directive('ehtDatepicker', EhtDatepicker);
})(angular.module('ngEhtCommons'));