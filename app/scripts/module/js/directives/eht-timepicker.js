(function(module){
	var EhtTimepicker = function(){
		return {
			'restrict' : 'A',
			'replace' : 'true',
			'require' : 'ngModel',
			'template' : '<div class="row">' +
			'	<div class="col-xs-{{12/fieldsCount}} form-group form-group-sm" ng-if="showHours">	' +
			'		<label class="col-xs-12 text-center">{{hoursLabel}}</label>		' +
			'		<select ng-model="hours" ' +
			'			class="form-control input-sm" ' +
			'			ng-options="hour for hour in range(23,0)">' +
			'			<option value="">{{hoursLabel}}</option>' +
			'		</select>' +
			'	</div>' +
			'	<div class="col-xs-{{12/fieldsCount}} form-group form-group-sm" ng-if="showMinutes">' +
			'		<label class="col-xs-12 text-center">{{minutesLabel}}</label>		' +
			'		<select ng-model="minutes" ' +
			'			class="form-control input-sm"' +
			'			ng-options="minute for minute in range(59, 0)">			' +
			'			<option value="">{{minutesLabel}}</option>' +
			'		</select>' +
			'	</div>' +
			'	<div class="col-xs-{{12/fieldsCount}} form-group form-group-sm" ng-if="showSeconds">' +
			'		<label class="col-xs-12 text-center">{{secondsLabel}}</label>			' +
			'		<select ng-model="seconds" ' +
			'			class="form-control input-sm" 	' +
			'			ng-options="second for second in range(59,0)">' +
			'			<option value="">{{secondsLabel}}</option>' +
			'		</select>' +
			'	</div>' +
			'	hours: {{hours}}' +
			'	<br/>' +
			'	minutes: {{minutes}}' +
			'	<br/>' +
			'	seconds: {{seconds}}' +
			'</div>',
			'scope' : {
				'format' : '@',
				'showHours' : '=',
				'showMinutes' : '=',
				'showSeconds' : '='
			},
			'link' : function(scope, element, attrs, NgModelController){
				scope.hoursLabel = attrs.hoursLabel ? attrs.hoursLabel : 'Hours';
				scope.minutesLabel = attrs.minutesLabel ? attrs.minutesLabel : 'Minutes';
				scope.secondsLabel = attrs.secondsLabel ? attrs.secondsLabel : 'Seconds';

				
				scope.range = function(size, starting){
					var values = [];
					var counter = starting;
					do{
						values.push(counter);
						counter++;
					}while(size >= counter);
					return values;
				};

				NgModelController.$formatters.push(function(modelValue){
					console.log('Formatting: ' + modelValue);
					var viewValue = {
						'hours' : 0,
						'minutes' : 0,
						'seconds' : 0
					};

					if(!modelValue) return viewValue;

					var time = moment(modelValue, scope.format);
					viewValue.hours = time.hours();
					viewValue.minutes = time.minutes();
					viewValue.seconds = time.seconds();

					console.log('View value: ' + angular.toJson(viewValue));
					return viewValue;
				});

				NgModelController.$render = function(){
					console.log('Rendering');
					//scope.hours = NgModelController.$viewValue.hours;
					//scope.minutes = NgModelController.$viewValue.minutes;
					//scope.seconds = NgModelController.$viewValue.seconds;
					console.log(NgModelController);
					console.log(angular.toJson(NgModelController));
					console.log('viewValue: ' + NgModelController.$viewValue);
					console.log('viewValue typeof: ' + (typeof NgModelController.$viewValue));
					console.log('viewValue toJson: ' + angular.toJson(NgModelController.$viewValue));
					console.log('viewValue.hours: ' + NgModelController.$viewValue.hours);
					console.log(scope.hours);
					console.log(scope.minutes);
					console.log(scope.seconds);
				};

				

				NgModelController.$parsers.push(function(viewValue){
					console.log('Parsing');
					var myMoment = moment();
					myMoment.hours(viewValue.hours);
					myMoment.minutes(viewValue.minutes);
					myMoment.seconds(viewValue.seconds);

					return myMoment.format(scope.format);

				});

				scope.fieldsCount = 0;
				if(scope.showHours) scope.fieldsCount++;
				if(scope.showMinutes) scope.fieldsCount++;
				if(scope.showSeconds) scope.fieldsCount++;

			}
		};
	};
	EhtTimepicker.$inject = [];
	module.directive('ehtTimepicker', EhtTimepicker);
})(angular.module('ngEhtCommons'));