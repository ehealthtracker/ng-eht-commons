(function(module){
	var EhtMap = function(){
		return {
			'scope' : {
				'style' : '@',
				'address' : '@'
			},
			'replace' : true,
			'restrict' : 'A',
			'template' : '' +
				'<div>' + 
					'<div class="eht-map" style="{{style}}"></div>' +
				'</div>'
			,
			'link' : function(scope, element, attrs){
				var mapOptions = {
		          zoom: 15
		        };

		        var $ehtMap = element.find('.eht-map');
		        var ehtMap = $ehtMap[0];

		        var map = new google.maps.Map(ehtMap, mapOptions);
		        var geocoder = new google.maps.Geocoder();

		        var marker = null;

		        var addressUpdated = function(){
		        	var address = scope.address;
		        	geocoder.geocode({
							'address': address
						}, 
						function(results, status) {
							if (status != google.maps.GeocoderStatus.OK) return; 
							map.setCenter(results[0].geometry.location);
							if(marker)
								marker.unbindAll();
							marker = new google.maps.Marker({
								map: map,
								position: results[0].geometry.location
							});
						}
					);
		        };
		        scope.$watch('address', function(){
		        	addressUpdated();
		        });
		        scope.$on('$destroy', function(){
		        	if(marker) marker.unbindAll();
		        	map.unbindAll();
		        });
			}
		}
	};
	EhtMap.$inject = [];
	module.directive('ehtMap', EhtMap);
})(angular.module('ngEhtCommons'));