(function(module){
	module.provider('logger', function() {
		var config = { loggers: { 'root': {level: 'INFO'} } };
		var useShortName = false;
		var createLogger = function($log) {
			var Logger = function(loggerName) {
				this.loggerName = loggerName;
				var level = config.loggers.root.level;
	
				var loggerNameParts = loggerName.split('.');
				this.shortLoggerName = loggerNameParts[loggerNameParts.length - 1];
				var counter = 0;
				var currentLoggerNameFragment = '';
				
				for(var index in loggerNameParts){
					var loggerNamePart = loggerNameParts[index];
					if (counter > 0) currentLoggerNameFragment += '.';
					currentLoggerNameFragment += loggerNamePart;
					if(typeof config.loggers[currentLoggerNameFragment] !== 'undefined'){
						var currentLoggerConfig = config.loggers[currentLoggerNameFragment];
						if (typeof currentLoggerConfig['level'] !== 'undefined') {
							level = currentLoggerConfig.level;
						}
					}
					counter++;
				}
				this.level = level;
			};
			Logger.prototype.isLevelEnabled = function(level) {
				var levelToCheckValue = this._getLevelValue(level);
				var currentValue = this._getLevelValue(this.level);
				return levelToCheckValue >= currentValue;
			};
			Logger.prototype.isFatalEnabled = function(){ return this.isLevelEnabled('FATAL'); };
			Logger.prototype.isWarnEnabled = function(){ return this.isLevelEnabled('WARN'); };
			Logger.prototype.isInfoEnabled = function(){ return this.isLevelEnabled('INFO'); };
			Logger.prototype.isDebugEnabled = function(){ return this.isLevelEnabled('DEBUG'); };
			Logger.prototype.isTraceEnabled = function(){ return this.isLevelEnabled('TRACE'); };
			Logger.prototype._getLevelValue = function(level) {
				var value = 0;
				switch (level) {
					case 'TRACE' : value = 1; break;
					case 'DEBUG' : value = 2; break;
					case 'INFO' : value = 3; break;
					case 'WARN' : value = 4; break;
					case 'FATAL' : value = 5; break;
				}
				return value;
			};
			Logger.prototype.warn = function(message) { this.log('WARN', message); };
			Logger.prototype.info = function(message) { this.log('INFO', message); };
			Logger.prototype.fatal = function(message) { this.log('FATAL', message); };
			Logger.prototype.debug = function(message) { this.log('DEBUG', message); };
			Logger.prototype.trace = function(message) { this.log('TRACE', message); };
	
			Logger.prototype.log = function(level, message) {
				var name = useShortName ? this.shortLoggerName : this.loggerName;
				if (this.isLevelEnabled(level)){
					var nameAndLevel = ' --- ' + name + ' [' + level + '] - ';
					var messages = [];
					if(typeof message === 'string' )
						messages.push(nameAndLevel + message);
					else{
						messages.push(nameAndLevel + '>>> Printing object <<<');
						messages.push(message);
					}
					angular.forEach(messages, function(msg){
						switch(level){
							case 'TRACE' :
							case 'DEBUG' : $log.debug(msg);break;
							case 'INFO' : $log.info(msg); break;
							case 'WARN' : $log.warn(msg); break;
							case 'FATAL' : $log.error(msg); break;
						}
					});
				}
			};
			return Logger;
		};
		return {
			setUseShortName : function(bool){
				useShortName = bool;       
			},
			configure: function(configuration) {
				for(var loggerName in configuration.loggers){
					var logger = configuration.loggers[loggerName];
					if (loggerName === 'root') {
						if(typeof logger.level !== 'undefined')
							config.loggers.root.level = logger.level;
					} else {
						config.loggers[loggerName] = logger;
					}
				}
			},
			$get: ['$log', function($log) {
				return createLogger($log);
			}]
		};
	});
})(angular.module('ngEhtCommons'));