(function(module){

	var getBaseJsonHeaders = function(){
		return {
			'Content-Type' : 'application/json',
			'Accept' : 'application/json'
		};
	};

	var EhtQuestionnairesCatalog = function($http, CacheFactory){

		var questionnairesCatalogCache = CacheFactory('questionnairesCatalogCache', {
	        maxAge: 10800000, // Items added to this cache expire after 3 horas.
	        cacheFlushInterval: 108000000, // This cache will clear itself every hour.
	        deleteOnExpire: 'aggressive', // Items will be deleted from this cache right when they expire.
	        storageMode: 'localStorage' // This cache will sync itself with `localStorage`.
	    });

		var getQuestionnaireCatalog = function(path){
			return $http({
				method : 'GET',
				url : '/ehealthtracker-rest/rest/medical_history' + path,
				cache : questionnairesCatalogCache,
				headers : getBaseJsonHeaders()
			});
		};

		var service={};

		service.getDiabetesQuestionnaire=function(){
			return getQuestionnaireCatalog('/diabetes_hra');
		};
		service.getHeartQuestionnaire=function(){
			return getQuestionnaireCatalog('/heart_hra');
		};
		return service;

	};

	EhtQuestionnairesCatalog.$inject = ['$http', 'CacheFactory'];
	module.factory('EhtQuestionnairesCatalog', EhtQuestionnairesCatalog);
})(angular.module('ngEhtCommons'));