(function(module){
	var getBaseJsonHeaders = function(){
		return {
			'Content-Type' : 'application/json',
			'Accept' : 'application/json'
		};
	};

	var EhtCatalogs = function($http, CacheFactory){

		var myAwesomeCache = CacheFactory('myAwesomeCache', {
	        maxAge: 10800000, // Items added to this cache expire after 3 horas.
	        cacheFlushInterval: 108000000, // This cache will clear itself every hour.
	        deleteOnExpire: 'aggressive', // Items will be deleted from this cache right when they expire.
	        storageMode: 'localStorage' // This cache will sync itself with `localStorage`.
	    });

		var getSimpleCatalog = function(path){
			return $http({
				method : 'GET',
				url : '/ehealthtracker-rest/rest/catalog' + path,
				cache : myAwesomeCache,
				headers : getBaseJsonHeaders()
			});
		};

		var getSimpleCatalogPost = function(path, data){
			return $http({
				method : 'POST',
				url : '/ehealthtracker-rest/rest/catalog' + path,
				cache : myAwesomeCache,
				data: data,
				headers : getBaseJsonHeaders()
			});
		};

		var service = {};
		service.getISOCountries = function(){
			return getSimpleCatalog('/iso-country');
		};
		service.getCountries = function(){
			return getSimpleCatalog('/country');
		};
		service.getStates = function(countryId){
			return getSimpleCatalog('/country/' + countryId + '/state');
		};
		service.getMexicanCities = function(stateId){
			return getSimpleCatalog('/state/' + stateId + '/city');
		};
		service.getMexicanLocalitiesAndNeighborhoods = function(stateId, cityId){
			return getSimpleCatalog('/state/' + stateId + '/city/' + cityId + '/neighborhood_and_locality');
		};
		service.getDataByMexicanZipCode = function(zipCode){
			return getSimpleCatalog('/zip_code/' + zipCode);
		};
		service.getNationalities = function(){
			return getSimpleCatalog('/nationality');
		};
		service.getReligions = function(){
			return getSimpleCatalog('/religion');
		};
		service.getMaritalStatuses = function(){
			return getSimpleCatalog('/marital_status');
		};
		service.getGenders = function(){
			return getSimpleCatalog('/gender');
		};
		service.getKinships = function(){
			return getSimpleCatalog('/relative');
		};
		service.getSports = function(){
			return getSimpleCatalog('/sport');
		};
		service.getTobaccoTypes = function(){
			return getSimpleCatalog('/tobacco-type');
		};
		service.getAlcoholicDrinks = function(){
            return getSimpleCatalog('/alcoholic_drinks');
        };
        service.getDrugs = function(){
        	return getSimpleCatalog('/drugs');
        };
        service.getWeightTypes = function(){
            return getSimpleCatalog('/weight-type');
        };
        service.getFeedTypes = function(){
            return getSimpleCatalog('/feed-type');
        };
        service.getCravingHours = function(){
            return getSimpleCatalog('/craving-hour');
        };
        service.getWeekTimes = function(){
            return getSimpleCatalog('/week-time');
        };
        service.getMedicalTestAnswers = function(){
            return getSimpleCatalog('/medical-test-answer');
        };
        service.getChildrenNumber = function(){
        	return getSimpleCatalog('/children-number');
        };
        service.getFreeAllergiesCategories = function(){
            return getSimpleCatalog('/free_allergy_category');
        };
        service.getAllergicReactions = function(){
            return getSimpleCatalog('/allergic_reactions');
        };
        service.getDisabilityGroups = function(){
            return getSimpleCatalog('/disability/groups');
        };
        service.getDisabilitySubgroups = function(groupId){
            return getSimpleCatalog('/disability/'+groupId+'/subgroups');
        };
        service.getDisabilitiesByGroupAndSubgroup = function(groupId, subgroupId){
            return getSimpleCatalog('/disability/'+groupId+'/'+subgroupId);
        };
        service.getCatKinship = function(){
            return getSimpleCatalog('/relative');
        };
        service.getCatHereditaryDiseases = function(){
            return getSimpleCatalog('/hereditary_diseases');
        };
        service.getCatPersonalDiseases = function(){
            return getSimpleCatalog('/personal_diseases');
        };
        service.getOtherDiseaseByHint = function(hint){
					var requestBody = {
						'query' : {
							'bool' : {
								'must' : []
							}
						}
					}

					angular.forEach(hint.split(" "), function(hint, index){

						var queryString = {
							'query_string' : {
								'query' : '*' + hint.toLowerCase() + '*',
								'fields' : ['name', 'name.folded']
							}
						};

						requestBody.query.bool.must.push(queryString);
					});

					return $http({
						'method' : 'POST',
						'url' : '/ehealthtracker-rest/rest/es/search-disease',
						'headers' : getBaseJsonHeaders(),
						'data' : requestBody
					});
        };
        service.getEnterprisesByUserId = function(userId){
			return getSimpleCatalog('/user/' + userId + '/enterprise');
		};
		service.getAllDiagnosis = function(){
			return getSimpleCatalog('/diagnosis');
		};
		service.getMedicinesNamesByHint = function(hint){
			return getSimpleCatalogPost('/medicines-names', {'value' : hint});
		};
		service.getMedicinesByHint = function(hint){
			return getSimpleCatalogPost('/medicines', {'value' : hint});
		};
		service.getMedicinesFromElasticSearchIndex = function(hint, hintSearch, selected){
			var requestBody = {
			    'query': {
			        'function_score': {
			            'query': {
			                'bool': {
			                    'must': []
			                }
			            },
			            'functions': [
			                {
			                    'filter': {
			                        'exists': {
			                            'field': 'enterprisesWithPrices'
			                        }
			                    },
			                    'weight': 2
			                }
			            ],
			            'score_mode': 'sum',
			            'boost_mode': 'multiply'
			        }
			    }
			};

			hint = hint.replace(/,/g, '');
			hint = hint.replace(/-/g, ' ');
			hint = hint.replace(/\//g, ' ');

			hintSearch = hintSearch.replace(/,/g, '');
			hintSearch = hintSearch.replace(/-/g, ' ');
			hintSearch = hintSearch.replace(/\//g, ' ');

			angular.forEach(hintSearch.split(' '), function(hintSearch, index){

				if(hintSearch != ''){
					var queryString = {
						'query_string' : {
							'query' : '*' + hintSearch.toLowerCase() + '*',
							'fields' : ['id', 'genericName', 'genericName.folded', 'tradename', 'tradename.folded']
						}
					};
					requestBody.
						query.
						function_score.
						query.
						bool.
						must.push(queryString);
				}
			});

			angular.forEach(hint.split(' '), function(hint, index){

				if(hint != ''){
					if(selected == true){
						var queryString = {
							'query_string' : {
								'query' : '*' + hint.toLowerCase() + '*',
								'fields' : ['id', 'genericName', 'genericName.folded', 'tradename', 'tradename.folded']
							},
							'query_string' : {
								'query' : hint.toLowerCase(),
								'fields' : ['id', 'genericName', 'genericName.folded', 'tradename', 'tradename.folded']
							}
						};
					} else {
						var queryString = {
							'query_string' : {
								'query' : '*' + hint.toLowerCase() + '*',
								'fields' : ['id', 'genericName', 'genericName.folded', 'tradename', 'tradename.folded']
							}
						};
					}
					requestBody.
						query.
						function_score.
						query.
						bool.
						must.push(queryString);
				}
			});

			return $http({
				'method' : 'POST',
				'url' : '/ehealthtracker-rest/rest/es/search-medicine',
				'headers' : getBaseJsonHeaders(),
				'data' : requestBody
			});
		};
		service.getBiomarkers = function (){
			return getSimpleCatalog('/biomarker');
		};
		service.getClinicalStudiesByHint = function (hint){
			return getSimpleCatalogPost('/clinical-studies/laboratories-and-gabinetes', {'value': hint});
		};
		service.getClinicalServicesByHint = function (hint){
			return getSimpleCatalogPost('/clinical-studies/services',{'value' : hint});
		};
		service.getClinicalLaboratoriesByHint = function (hint){
			return getSimpleCatalogPost('/clinical-studies/laboratories',{'value' : hint});
		};
		service.getClinicalGabinetssByHint = function (hint){
			return getSimpleCatalogPost('/clinical-studies/gabinets', {'value' : hint});
		};
		service.getPRDsByHint = function (hint){
			return getSimpleCatalogPost('/clinical-studies/prds', {'value' : hint});
		};
		service.getFrequencies = function (){
			return getSimpleCatalog('/frequencies');
		};
		service.getClinicalTestsByHint = function (hint){
			return getSimpleCatalogPost('/clinical-studies', {'value' : hint});
		};
		service.getActiveProtocols = function (){
			return getSimpleCatalog('/protocol');
		};
		service.getClinicsByUserId = function (userId){
			return getSimpleCatalog('/user/' + userId + '/clinic');
		};
		service.getEnterprisesByClinicId = function (clinicId){
			return getSimpleCatalog('/clinic/' + clinicId + '/enterprise');
		};
		service.getCampaigns = function (){
			return getSimpleCatalog('/campaigns');
		};
		service.getClinics = function(){
			return getSimpleCatalog('/clinic');
		};
		service.getSitesByEnterpriseId = function(enterpriseId){
			return getSimpleCatalog('/enterprise/' + enterpriseId +'/site');
		};
		service.getCheckupsByHint = function (hint){
			return getSimpleCatalogPost('/checkup', {'value' : hint});
		};
		service.getStaffByHintAndRole = function (hint, roles){
			return getSimpleCatalogPost('/staff', {'searchData' : hint, 'searchRoles' : roles});
		};
		service.getCatNotification = function (){
			return getSimpleCatalog('/notifications');
		};
		service.getCampaignNotificationTemplates = function (tag){
			return getSimpleCatalog('/campaign-notification-templates?tag=' + tag);
		};
		service.getCheckupList = function (){
			return getSimpleCatalog('/checkup');
		};
		service.getCPTGroups = function (){
			return getSimpleCatalog('/cpt-group');
		};
		service.getCPTs = function (groupId){
			return getSimpleCatalog('/cpt-group/' + groupId + "/cpt");
		};
		service.getMandrillTagsByHint = function(key){
			return getSimpleCatalog('/campaign-tags?key=' + key);
		}
		return service;
	};

	EhtCatalogs.$inject = ['$http', 'CacheFactory'];
	module.factory('EhtCatalogs', EhtCatalogs);
})(angular.module('ngEhtCommons'));
