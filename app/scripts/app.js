'use strict';

/**
 * @ngdoc overview
 * @name ngEhtCommonsApp
 * @description
 * # ngEhtCommonsApp
 *
 * Main module of the application.
 */
angular
  .module('ngEhtCommonsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngEhtCommons'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/map', {
        templateUrl: 'views/map.html',
        controller: 'MapCtrl'
      })
      .when('/datepicker', {
        templateUrl: 'views/datepicker.html',
        controller: 'DatepickerCtrl'
      })
      .when('/dropdown-datepicker', {
        templateUrl: 'views/dropdown-datepicker.html',
        controller: 'DropdownDatepickerCtrl'
      })
      .when('/catalogs', {
        templateUrl: 'views/catalogs.html',
        controller: 'CatalogsCtrl'
      })
      .when('/autocomplete', {
        templateUrl: 'views/autocomplete.html',
        controller: 'AutocompleteCtrl'
      })
      .when('/capitalize', {
        templateUrl: 'views/capitalize.html',
        controller: 'CapitalizeCtrl'
      })
      .when('/timepicker', {
        templateUrl: 'views/timepicker.html',
        controller: 'TimepickerCtrl'
      })
      .when('/tooltip', {
        templateUrl: 'views/tooltip.html',
        controller: 'TooltipCtrl'
      })
      .when('/dropzone', {
        templateUrl: 'views/dropzone.html',
        controller: 'DropzoneCtrl'
      })
      .when('/chosen', {
        templateUrl: 'views/chosen.html',
        controller: 'ChosenCtrl'
      })
      .when('/eht-enter-press', {
        templateUrl: 'views/eht-enter-press.html',
        controller: 'EhtEnterPresCtrl'
      })
      .when('/clockpicker', {
        templateUrl: 'views/clockpicker.html',
        controller: 'ClockpickerCtrl'
      })
      .when('/advanced-filter', {
        templateUrl: 'views/advanced-filter.html',
        controller: 'AdvancedFilterCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
